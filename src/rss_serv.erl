-module(rss_serv).

-behaviour(gen_server).

%% API
-export([start_link/3, start_link/4, stop/1, feed/1, exists/1]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-type entry() :: [ {title, iodata()} | {description, iodata()} | {guid, iodata()}
                 | {link, iodata()} | {pubdate, iodata()}, ...].
-record(state, {title :: iodata(),
                description :: iodata(),
                interval :: pos_integer(),
                entries=[] :: [entry()],
                tref :: reference()}).

-export_type([entry/0]).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link(Name, Title, Description) ->
    {ok, Delay} = application:get_env(rss_generator, min_delay),
    start_link(Name, Title, Description, Delay).

start_link(Name, Title, Description, Delay) when is_integer(Delay), Delay >= 1000 ->
    gen_server:start_link({via, gproc, {n,l,Name}}, ?MODULE,
                          {Title,Description,Delay}, []).

stop(Name) ->
    gen_server:call({via, gproc, {n,l,Name}}, stop).

-spec feed(Name::term()) -> [ {title, iodata()} | {description, iodata()}
                            | {ttl, pos_integer()} | {pubdate, iodata()}
                            | {items, [entry()]}, ... ].
feed(Name) ->
    gen_server:call({via, gproc, {n,l,Name}}, feed).

exists(Name) ->
    case gproc:whereis_name({n,l,Name}) of
        undefined -> false;
        _ -> true
    end.

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init({Title, Desc, Delay}) ->
    Tref = start_timer(Delay),
    {ok, #state{title=Title,
                description=Desc,
                interval=Delay,
                tref=Tref}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @spec handle_call(Request, From, State) ->
%%                                   {reply, Reply, State} |
%%                                   {reply, Reply, State, Timeout} |
%%                                   {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_call(stop, _From, State) ->
    {stop, shutdown, ok, State};
handle_call(feed, _From, S=#state{title=T,description=D,entries=Items,
                                  interval=TTL}) ->
    Reply = case Items of
        [] ->
            [{title,T}, {description,D}, {ttl,TTL},
             {pubdate, pubdate()}, {items,[]}];
        [Newest|_] ->
            [{title,T}, {description,D}, {ttl,TTL},
             {pubdate,proplists:get_value(pubdate,Newest)},
             {items,Items}]
    end,
    {reply, Reply, S};
handle_call(_Request, _From, State) ->
    {reply, noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State) -> {noreply, State} |
%%                                  {noreply, State, Timeout} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_cast(_Msg, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_info({timeout, Tref, generate}, S=#state{tref=Tref, entries=Items,
                                                interval=Delay}) ->
    Title=title(),
    Entry = [{title, Title},
             {description, description()},
             {guid, guid()},
             {link, [$/, sluggify(Title)]},
             {pubdate, pubdate()}],
    NewTref = start_timer(Delay),
    {noreply, S#state{tref=NewTref,
                      entries=lists:sublist([Entry|Items],max_entries())}};
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
start_timer(Delay) ->
    erlang:start_timer(Delay, self(), generate).

max_entries() ->
    {ok, Max} = application:get_env(rss_generator, max_entries),
    Max.

title() ->
    io_lib:format("some-random-words-~p", [now()]).

description() ->
    io_lib:format("my random description of some text ~p.", [now()]).

guid() ->
    base64:encode(crypto:hash(md5, term_to_binary({self(), now()}))).

sluggify(Str) ->
    Patterns = [{"[âäàáÀÄÂÁ]", "a"},
               {"[éêëèÉÊËÈ]", "e"},
               {"[ïîíìÏÎÌÍ]", "i"},
               {"[öôòóÖÔÒÓ]", "o"},
               {"[üûùúÜÛÙÚ]", "u"},
               {"[ÿýÝ]", "y"},
               {"[çÇ]", "c"},
               {"[^\\w\\d_-]", "-"},
               {"[-]{2,}", "-"},
               {"(?:^-)|(?:-$)", ""}],
    Slug = lists:foldl(fun({Pat, Rep}, Slug) ->
                         re:replace(Slug, Pat, Rep, [global, {return, binary}])
                       end,
                       Str,
                       Patterns),
    string:to_lower(binary_to_list(Slug)).

pubdate() ->
    dh_date:format("D, d M Y h:i:s +0000").
