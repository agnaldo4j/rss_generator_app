-module(rss_generator).
-export([start_feed/3, start_feed/4, get_feed/1, exists/1]).

start_feed(Name, Title, Description) ->
    supervisor:start_child(rss_sup, [Name, Title, Description]).

start_feed(Name, Title, Description, Delay) ->
    supervisor:start_child(rss_sup, [Name, Title, Description, Delay]).

get_feed(Name) ->
    rss_serv:feed(Name).

exists(Name) ->
    rss_serv:exists(Name).

